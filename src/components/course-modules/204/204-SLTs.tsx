import SLTsItems from "@/src/components/lms/Lesson/SLTs";
import { Divider, Heading, Box, Center, Grid, Text, Link as CLink } from "@chakra-ui/react";
import React from "react";
import Introduction from "@/src/components/course-modules/204/Introduction.mdx";
import LessonNavigation from "@/src/components/lms/Lesson/LessonNavigation";
import VideoComponent from "../../lms/Lesson/VideoComponent";
import LiteYouTubeEmbed from "react-lite-youtube-embed";
import Link from "next/link";

const SLTs204 = () => {
  return (
    <Box w="95%" marginTop="2em">
      <Grid templateColumns="repeat(2, 1fr)" gap={5}>
        <SLTsItems moduleTitle="Module 204" moduleNumber={204} />
        <Box>
          <VideoComponent videoId="esCBPAAJFTQ">About This Module</VideoComponent>
        </Box>
      </Grid>
      <Divider mt="5" />
      <Box w="60%" mx="auto">
        <Text py="5" fontSize="xl">
          This project is your opportunity to apply the most important skills you have learned so far in this course. By
          completing it, you will show that you are ready to start contributing to development projects that will be
          introduced in the 300-Level Modules. When you do, you will start to specialize in different parts of the
          Cardano development stack.
        </Text>
        <Text py="5" fontSize="xl">
          Think about what excites you most as you complete this project. What do you want to focus on as we continue to
          build?
        </Text>

        <Text py="5" fontSize="xl">
          At{" "}
          <Link href="/live-coding">
            <CLink>Live Coding</CLink>
          </Link>{" "}
          on 2023-08-02 and 2023-08-03, we took a deeper dive into the project. You can review the videos here:
        </Text>
        <Heading size="lg" py="5">
          Live Coding - 2023-08-02
        </Heading>
        <LiteYouTubeEmbed id="NhBIIpZBy4s" title="example video" />
        <Heading size="lg" py="5">
          Live Coding - 2023-08-03
        </Heading>
        <LiteYouTubeEmbed id="i2uyLZq4REQ" title="example video" />
      </Box>
      <Box py="5">
        <Introduction />
      </Box>
      <LessonNavigation moduleNumber={204} currentSlug="slts" />
    </Box>
  );
};

export default SLTs204;
