import Assignment2031 from "@/src/components/course-modules/203/Assignment2031.mdx";
import CommitLayout from "@/src/components/lms/Lesson/CommitLayout";
import { Grid, GridItem, Text, Box, Heading } from "@chakra-ui/react";
import CommitmentTx from "@/src/components/gpte/transactions/CommitmentTx";
import { useContext, useEffect, useState } from "react";
import { PPBLContext } from "@/src/context/PPBLContext";
import Link from "next/link";
import { useWallet } from "@meshsdk/react";
import { getInlineDatumForContributorReference } from "@/src/data/queries/getInlineDatumForContributorReference";
import { checkReferenceDatumForPrerequisite } from "@/src/utils";
import NFTGallery from "./cardano/NFTGallery";

const NFTGalleryPage = () => {
  

  return (
    <>  
      <NFTGallery />
      <CommitLayout moduleNumber={203} slug="nft-gallery" />
    </>
  );
};

export default NFTGalleryPage;
